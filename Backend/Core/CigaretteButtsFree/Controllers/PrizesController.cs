﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CigaretteButtsFree.Dtos;
using CigaretteButtsFree.Entities;
using CigaretteButtsFree.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CigaretteButtsFree.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class PrizesController : ControllerBase
    {

        private readonly CigaretteButtsFreeDbContext dbContext;
        private readonly ApplicationUser currentUser;

        public PrizesController(CigaretteButtsFreeDbContext dbContext, UserResolverService userResolverService)
        {
            this.dbContext = dbContext;
            currentUser = userResolverService.GetUser();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Buy([FromBody]BuyPrizeDto prizeToBuy)
        {
            if (ModelState.IsValid && currentUser != null)
            {
                try
                {
                    var currentPrize = dbContext.Prizes.FirstOrDefault(x => x.PrizeId == prizeToBuy.PrizeId);

                    if (currentUser.CurrentPoints < currentPrize.Price)
                    {
                        return BadRequest("Not enough funds on the user account!");
                    }
                    else
                    {
                        currentUser.CurrentPoints -= currentPrize.Price;

                        var newBought = new UserPrizeBought()
                        {
                            PrizeId = prizeToBuy.PrizeId,
                            UserId = currentUser.Id,
                            Fulfilled = false
                        };

                        dbContext.Bought.Add(newBought);

                        await dbContext.SaveChangesAsync();

                        return Ok("You have bought the product");
                    }
                }
                catch
                {
                    return BadRequest("Couldn't find such prize.");
                }
            }
            return BadRequest("Bad request in Prizes/Buy");
        }

        [Authorize]
        [HttpGet]
        public IActionResult Get()
        {
            if (ModelState.IsValid && currentUser != null)
            {
                try
                {
                    return Ok(dbContext.Prizes.ToList());
                }
                catch
                {
                    return BadRequest("Couldn't find such prize.");
                }
            }
            return BadRequest("Bad request in Prizes/Buy");
        }

        [Authorize(Roles = Role.Employee + "," + Role.Admin)]
        [HttpPost]
        public async Task<IActionResult> Add([FromBody]AddPrizeDto prizeToAdd)
        {
            if (ModelState.IsValid && currentUser != null)
            {
                try
                {
                    var newPrize = new Prize()
                    {
                        Name = prizeToAdd.Name,
                        ImageUrl = prizeToAdd.ImageUrl,
                        Price = prizeToAdd.Price,
                        Description = prizeToAdd.Description,
                        MaxPerUser = prizeToAdd.MaxPerUser
                    };

                    dbContext.Prizes.Add(newPrize);

                    await dbContext.SaveChangesAsync();

                    return Ok("You have added a new prize!");
                }
                catch
                {
                    return BadRequest("Couldn't find such prize.");
                }
            }
            return BadRequest("Bad request in Prizes/Buy");
        }

        [Authorize(Roles = Role.Employee + "," + Role.Admin)]
        [HttpGet]
        public IActionResult GetBought()
        {
            if (ModelState.IsValid && currentUser != null)
            {
                try
                {
                    return Ok(dbContext.Bought.ToList());
                }
                catch
                {
                    return BadRequest("Couldn't find bought.");
                }
            }
            return BadRequest("Bad request in bought prizes.");
        }
    }
}
