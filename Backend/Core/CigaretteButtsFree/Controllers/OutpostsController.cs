﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CigaretteButtsFree.Dtos;
using CigaretteButtsFree.Entities;
using CigaretteButtsFree.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CigaretteButtsFree.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class OutpostsController : ControllerBase
    {


        private readonly CigaretteButtsFreeDbContext dbContext;
        private readonly ApplicationUser currentUser;

        public OutpostsController(CigaretteButtsFreeDbContext dbContext, UserResolverService userResolverService)
        {
            this.dbContext = dbContext;
            currentUser = userResolverService.GetUser();
        }

        [Authorize]
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var allOutposts = dbContext.Outposts;
                return Ok(allOutposts);
            }
            catch
            {
                return BadRequest("Bad request at get all outposts.");
            }
        }

        [Authorize(Roles = Role.Employee+","+Role.Admin)]
        [HttpPost]
        public async Task<IActionResult> Add([FromBody]AddOutpostDto outpostToAddDto)
        {
            try
            {
                var newOutpost = new Outpost() {
                    Latitude = outpostToAddDto.Latitude,
                    Longitude = outpostToAddDto.Longitude
                };
                dbContext.Outposts.Add(newOutpost);
                await dbContext.SaveChangesAsync();
                return Ok();
            }
            catch
            {
                return BadRequest("Bad Request in Outpost/Add");
            }
        }
    }
}
