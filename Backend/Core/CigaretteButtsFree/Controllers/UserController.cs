﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Castle.Core.Configuration;
using CigaretteButtsFree.Entities;
using CigaretteButtsFree.Services;
using CigaretteButtsFree.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CigaretteButtsFree.Controllers
{

    [ApiController]
    [Route("api/[controller]/[action]")]
    public class UserController : Controller
    {
        const string qrCodeUrl = "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=";

        private readonly ApplicationUser applicationUser;
        private readonly CigaretteButtsFreeDbContext dbContext;

        public UserController(
            CigaretteButtsFreeDbContext dbContext,
            UserResolverService userResolverService
            )
        {
            this.dbContext = dbContext;
            applicationUser = userResolverService.GetUser();
        }

        [Authorize(Roles = Role.User)]
        [HttpGet]
        public IActionResult GetUserData()
        {
            try
            {
                MainUserViewmodel viewmodel = new MainUserViewmodel
                {
                    Name = applicationUser.Name,
                    Surname = applicationUser.Surname,
                    SumOfPoints = applicationUser.CurrentPoints,
                    SumOfCollectedPoints = applicationUser.SumOfCollectedPoints,
                    QrCodeUrl = qrCodeUrl + applicationUser.Id
                };
                return Ok(viewmodel);
            }
            catch (Exception x)
            {
                return BadRequest(x);
            }
        }
    }
}