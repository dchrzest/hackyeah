﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CigaretteButtsFree.Dtos;
using CigaretteButtsFree.Entities;
using CigaretteButtsFree.Services;
using CigaretteButtsFree.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CigaretteButtsFree.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class PointsController : ControllerBase
    {
        private readonly CigaretteButtsFreeDbContext dbContext;
        private readonly ApplicationUser currentUser;

        public PointsController(CigaretteButtsFreeDbContext dbContext, UserResolverService userResolverService)
        {
            this.dbContext = dbContext;
            currentUser = userResolverService.GetUser();
        }

        [Authorize(Roles = Role.Admin + "," + Role.Employee)]
        [HttpPost]
        public async Task<IActionResult> Add([FromBody]AddPointsDto pointsToAdd)
        {
            if (ModelState.IsValid)
            {
                if (pointsToAdd.UserId == null)
                {
                    return NotFound();
                }
                try
                {
                    var userToUpdate = await dbContext.Users.FirstOrDefaultAsync(x => x.Id == pointsToAdd.UserId);

                    userToUpdate.CurrentPoints += pointsToAdd.Points;
                    userToUpdate.SumOfCollectedPoints += pointsToAdd.Points;

                    await dbContext.SaveChangesAsync();
                }
                catch
                {
                    return BadRequest("Wrong data send through Points/Add method");
                }
            }

            return Ok();
        }

        [Authorize]
        [HttpGet]
        public IActionResult UserPoints()
        {
            try
            {
                var userPointsViewModel = new UserPointDataViewModel()
                {
                    CurrentPoints = currentUser.CurrentPoints,
                    OverallPoints = currentUser.SumOfCollectedPoints
                };
                return Ok(userPointsViewModel);
            }
            catch
            {
                return BadRequest("Couldn't get current user points or overall points.");
            }
        }
    }
}