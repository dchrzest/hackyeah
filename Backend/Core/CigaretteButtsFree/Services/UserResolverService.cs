﻿using CigaretteButtsFree.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CigaretteButtsFree.Services
{
    public class UserResolverService
    {
        private readonly IHttpContextAccessor context;
        private readonly UserManager<ApplicationUser> userManager;

        public UserResolverService(IHttpContextAccessor context, UserManager<ApplicationUser> userManager)
        {
            this.context = context;
            this.userManager = userManager;
        }

        public ApplicationUser GetUser()
        {
            var id = context.HttpContext.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            return id == null ? null : userManager.FindByIdAsync(id).GetAwaiter().GetResult();
        }
    }
}
