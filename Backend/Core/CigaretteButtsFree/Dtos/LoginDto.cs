﻿using System.ComponentModel.DataAnnotations;

namespace CigaretteButtsFree.Web.Dtos
{
    public class LoginDto
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
