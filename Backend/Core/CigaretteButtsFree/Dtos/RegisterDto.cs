﻿using System;
using System.ComponentModel.DataAnnotations;
using CigaretteButtsFree.Entities;

namespace CigaretteButtsFree.Web.Dtos
{
    public class RegisterDto
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "PASSWORD_MIN_LENGTH", MinimumLength = 6)]
        public string Password { get; set; }

        public String Role { get; set; }
    }
}
