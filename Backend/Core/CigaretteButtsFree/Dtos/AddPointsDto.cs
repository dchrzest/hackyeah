﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CigaretteButtsFree.Dtos
{
    public class AddPointsDto
    {
        public string UserId { get; set; }
        public int Points { get; set; }
    }
}
