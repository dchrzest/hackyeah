﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CigaretteButtsFree.Dtos
{
    public class AddOutpostDto
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
