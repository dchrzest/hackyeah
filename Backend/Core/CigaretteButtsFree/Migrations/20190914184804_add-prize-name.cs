﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CigaretteButtsFree.Migrations
{
    public partial class addprizename : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Prizes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Prizes");
        }
    }
}
