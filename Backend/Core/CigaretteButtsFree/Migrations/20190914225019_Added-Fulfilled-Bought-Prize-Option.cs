﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CigaretteButtsFree.Migrations
{
    public partial class AddedFulfilledBoughtPrizeOption : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Fulfilled",
                table: "Bought",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Fulfilled",
                table: "Bought");
        }
    }
}
