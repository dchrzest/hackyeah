﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CigaretteButtsFree.Entities
{
    public class Prize
    {
        public long PrizeId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public int Price { get; set; }
        public string Description { get; set; }
        public int MaxPerUser { get; set; } // 0 = unlimited buys
    }
}
