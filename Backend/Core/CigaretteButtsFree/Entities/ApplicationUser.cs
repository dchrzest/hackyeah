﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CigaretteButtsFree.Entities
{
    public enum AuthenticationType
    {
        User, Admin, Employee
    }

    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Role { get; set; }
        public int CurrentPoints { get; set; }
        public int SumOfCollectedPoints { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
