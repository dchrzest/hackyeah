﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CigaretteButtsFree.Entities
{
    public class UserPrizeBought
    {
        public long UserPrizeBoughtId { get; set; }
        public string UserId { get; set; }
        public long PrizeId { get; set; }
        public bool Fulfilled { get; set; }
    }
}
