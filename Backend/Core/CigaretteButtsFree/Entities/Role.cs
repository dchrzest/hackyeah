﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CigaretteButtsFree.Entities
{
    public static class Role
    {
        public const string User = "User";
        public const string Admin = "Admin";
        public const string Employee = "Employee";
    }
}
