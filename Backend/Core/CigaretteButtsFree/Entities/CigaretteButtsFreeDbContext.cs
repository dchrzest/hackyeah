﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CigaretteButtsFree.Entities
{
    public class CigaretteButtsFreeDbContext : IdentityDbContext<ApplicationUser>
    {
        public CigaretteButtsFreeDbContext(DbContextOptions<CigaretteButtsFreeDbContext> options) : base(options)
        {

        }

        public virtual DbSet<Prize> Prizes { get; set; }
        public virtual DbSet<Outpost> Outposts { get; set; }

        public virtual DbSet<UserPrizeBought> Bought { get; set; }

    }
}
