﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CigaretteButtsFree.Entities
{
    public class Outpost
    {
        public int OutpostId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
