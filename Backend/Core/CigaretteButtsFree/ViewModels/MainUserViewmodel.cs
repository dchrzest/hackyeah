﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CigaretteButtsFree.ViewModels
{
    public class MainUserViewmodel
    {
        public string QrCodeUrl { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int SumOfPoints { get; set; }
        public int SumOfCollectedPoints { get; set; }
    }
}
