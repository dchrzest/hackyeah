﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CigaretteButtsFree.ViewModels
{
    public class UserPointDataViewModel
    {
        public int CurrentPoints { get; set; }
        public int OverallPoints { get; set; }
    }
}
